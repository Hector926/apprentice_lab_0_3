class User < ActiveRecord::Base
  has_many :projects
  has_many :solutions

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
