class SolutionsController < ApplicationController
  before_action :find_solution, except:[:new, :create, :index]

  def index
    @solutions = Solution.all
  end

  def show

  end

  def new
    puts "PARAMS == #{params}"
    @solution = Solution.new
    @project = Project.find(params[:project_id])
  end

  def create
    @solution = current_user.solutions.new(solution_params)

    puts "solutions == #{@solutions}\n\n\n"
    if @solution.save
      flash[:notice] = 'Solutions Created'
      redirect_to projects_path
    end


  end

  def edit
  end

  def update
  end

  def destroy

  end

  private

  def solution_params
    params.require(:solution).permit(:url, :user_id, :project_id)
  end

  def find_solution
    @solution = Solution.find(param[:id])
  end

  def build_solution_hash(params)
    {
     url: params['solution']['url'],
     user_id: current_user.id,
     project_id: params['project_id'].to_i
    }
  end


end
