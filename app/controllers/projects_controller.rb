class ProjectsController < ApplicationController
  before_action :find_project, except:[:new, :index, :create]

  def index
    @projects = Project.all
  end

  def new
    @project = Project.new
  end

  def create
    @project = current_user.projects.new(project_params)

    if @project.save
      flash[:notice] = 'Project created successfully'
      redirect_to @project
    else
      flast[:error] = 'Something went wrong'
      render :new
    end

  end

  def edit

  end

  def show
  end

  def update
    if @project.update(project_params)
      redirect_to @project
    else
      render :update
    end
  end

  def destroy
  end


  private

  def find_project
    @project = Project.find(params[:id])
  end

  def project_params
    params.require(:project).permit(:title, :description, :user_id)
  end


end
