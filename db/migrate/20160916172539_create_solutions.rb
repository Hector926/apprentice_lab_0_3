class CreateSolutions < ActiveRecord::Migration
  def change
    create_table :solutions do |t|
      t.string :url
      t.integer :user_id
      t.integer :project_id

      t.timestamps null: false
    end
  end
end
